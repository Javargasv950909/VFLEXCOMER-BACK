const express = require('express');
require('dotenv').config();
var cors = require('cors');
const { dbConnection } = require('./Database/config');

// se inicializa el servidor de express
const app = express();

// configuracion del cors
app.use(cors());


//Lectura y parseo del body
app.use(express.json());

// base de datos
dbConnection();

//rutas

app.get('/', (req, res) => {
    res.json({
        ok: true,
        msg: 'Inicio del servidor correcto'
    });
});


// RUTAS PARA PRODUCTOS

app.use('/api/productos', require('./Routes/productos'));
app.use('/api/nuevoProducto', require('./Routes/productos'));
app.use('/api/eliminarProducto', require('./Routes/productos'));
app.use('/api/actualizarProducto', require('./Routes/productos'));

// RUTAS USUARIOS

app.use('/api/planes', require('./Routes/planes'));
app.use('/api/usuarios', require('./Routes/usuarios'));
app.use('/api/tiendas', require('./Routes/tiendas'));
app.use('/api/login', require('./Routes/auth'));

// RUTAS PARA COMPRAS

app.use('/api/compras', require('./Routes/compras'));

// RUTAS PARA VENTAS

app.use('/api/ventas', require('./Routes/ventas'));
app.use('/api/nuevaVenta', require('./Routes/ventas'));

// RUTAS PARA PROVEEDORES

app.use('/api/proveedores', require('./Routes/proveedores'));
app.use('/api/nuevoProveedor', require('./Routes/proveedores'));

app.listen(process.env.PORT, () => {
    console.log("Servidor corriendo exitosamente en el puerto " + process.env.PORT);
});