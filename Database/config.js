const mongoose = require('mongoose');
require('dotenv').config();

const dbConnection = () => {

    mongoose.connect(process.env.DB_CNN, {
        useNewUrlParser: true,
        useUnifiedTopology: true,
        useCreateIndex: true,
        useFindAndModify: false
    });
    const db = mongoose.connection;

    db.once('open', function() {
        console.log("Conectado a la base de datos de VF");
    });

    db.on('unhandledRejection', (reason, promise) => {
        console.log('Entre al Rejection:', promise, 'reason:', reason);
        throw new Error('Error en la conexion de la DB');
    });

}

module.exports = {
    dbConnection
}