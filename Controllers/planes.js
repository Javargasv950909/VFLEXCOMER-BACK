const { response } = require('express');
const Plan = require('../Models/plan');

const crearPlanes = async(req, res = response) => {

    const { nombre, descripcion } = req.body;
    try {
        const plan = new Plan(req.body);

        //Guardar plan
        await plan.save();
        res.json({
            ok: true,
            plan
        });
    } catch (error) {
        console.log(error);
        res.status(500).json({
            ok: false,
            msg: 'Error inesperado, -> crear planes... ver logs'
        })
    }
}


const getPlanes = async(req, res) => {

    const planes = await Plan.find({}, 'id nombre descripcion');
    res.json({
        ok: true,
        planes,
        id: req.id
    });
}

const eliminarPlanes = async(req, res = response) => {

    const planid = req.params.id;

    try {
        //Busqueda de plan por ID
        const planDB = await Plan.findById(planid);

        if (!planDB) {
            return res.status(400).json({
                ok: false,
                msg: 'No existe un plan con este ID'
            });
        }

        //Eliminar plan de BD
        await Plan.findByIdAndDelete(planid);
        res.status(500).json({
            ok: true,
            msg: 'Plan eliminado correctamente'
        });

    } catch (error) {
        res.status(500).json({
            ok: false,
            msg: 'No se pudo eliminar el Plan... ver logs'
        });
    }
}

module.exports = {
    getPlanes,
    crearPlanes,
    eliminarPlanes
}