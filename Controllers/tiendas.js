const { response } = require('express');
const Tienda = require('../Models/tienda');


const crearTienda = async(req, res = response) => {

    const {
        fecha_registro,
        nombre_tienda,
        descripcion_tienda,
        direccion_tienda,
        departamento,
        ciudad,
        nit,
        estado_tienda
    } = req.body;

    try {

        // Validacion existe tienda
        const existeTienda = await Tienda.findOne({ nit });

        if (existeTienda) {
            return res.status(400).json({
                ok: false,
                msg: 'Establecimiento ya registrado'
            });
        }

        const nuevaTienda = new Tienda(req.body);

        //Guardar tienda
        await nuevaTienda.save();

        res.json({
            ok: true,
            nuevaTienda
        });

    } catch (error) {
        console.log(error);
        res.status(500).json({
            ok: false,
            msg: 'Error inesperado  --> crear Tienda... ver logs'
        })
    }
}

const getTiendas = async(req, res) => {

    const tiendas = await Tienda.find({}, 'id fecha_registro nombre_tienda descripcion_tienda departamento ciudad nit estado_tienda');
    res.json({
        ok: true,
        tiendas,
        id: req.id,
    });
}


const actualizarTienda = async(req, res = response) => {

    const tiendaid = req.params.id;

    try {

        // Busqueda de usuario por id
        const tiendaDB = await Tienda.findById(tiendaid);

        if (!tiendaDB) {
            return res.status(400).json({
                ok: false,
                msg: 'No existe un establecimiento con este ID'
            });
        }

        //Campos a actualizar
        const { fecha_registro, nit, ...campos } = req.body;

        if (tiendaDB.nit != nit) {
            const existeTienda = await Tienda.findOne({ nit });
            if (existeTienda) {
                return res.status(400).json({
                    ok: false,
                    msg: 'Ya existe un establecimiento con ese nit'
                });
            }

        }

        // Actualizar tienda 
        const tiendaActualizada = await Tienda.findByIdAndUpdate(tiendaid, campos, { new: true });

        res.json({
            ok: true,
            usuario: tiendaActualizada,
            msg: "Tienda actualizada correctamente."
        });

    } catch (error) {
        res.status(500).json({
            ok: false,
            msg: 'Error inesperado  --> actualizar Tienda... ver logs'
        });
    }
}

const eliminarTienda = async(req, res = response) => {

    const tiendaid = req.params.id;

    console.log(tiendaid);

    try {
        //Busqueda de tienda por ID
        const tiendaDB = await Tienda.findById(tiendaid);

        if (!tiendaDB) {
            return res.status(400).json({
                ok: false,
                msg: 'No existe un establecimiento con este ID'
            });
        }

        //Eliminar establecimiento de BD
        /*
        await Usuario.findByIdAndDelete(usuarioid);
        res.status(500).json({
            ok: true,
            msg: 'establecimiento eliminado correctamente'
        });*/

        // Actualizar estado del establecimiento
        await Tienda.findOneAndUpdate({ _id: tiendaid }, { estado_tienda: false }, { new: true });
        res.status(500).json({
            ok: true,
            msg: 'Establecimiento eliminado correctamente'
        });

    } catch (error) {
        res.status(500).json({
            ok: false,
            msg: 'No se pudo eliminar el establecimiento... ver logs'
        });
    }
}


module.exports = {
    getTiendas,
    crearTienda,
    eliminarTienda,
    actualizarTienda
}