const { response } = require('express');
const Producto = require('../Models/producto');


const crearProducto = async(req, res = response) => {
    const { id_tienda, nombre, bc, cantidad, precio } = req.body;

    try {
        const producto = new Producto(req.body);

        const existeProd = await Producto.findOne({ nombre });

        if (existeProd) {
            return res.status(400).json({
                ok: false,
                msg: 'Este producto ya existe'
            })
        }

        await producto.save(producto);
        res.json({
            ok: true,
            producto
        });
    } catch (error) {
        res.status(500).json({
            ok: false,
            msg: 'Ocurrio un error, consultar Logs'
        })
    }
}

const getProductos = async(req, res) => {

    try {
        const productos = await Producto.find({}, 'id id_tienda bc nombre cantidad precio')
            res.json({
                ok: true,
                productos,
                id: req.id
            });
    } catch (error) {
        res.status(500).json({
            ok: false,
            msg: 'Hubo un error en el servidor'
        })
    }
}


const eliminarProducto = async(req, res) => {

    try {

        const id = req.params.id;
        await Producto.findByIdAndDelete(id);
        res.json({
            ok: true,
            msg: 'Producto eliminado'
        });

    } catch (error) {
        res.status(500).json({
            ok: false,
            msg: 'Hubo un error en el servidor'
        })
    }
}

const actualizarProducto = async(req, res) => {

    const id = req.params.id;

    try {

        const producto = await Producto.findById(id);

        if (!producto) {
            return res.status(404).json({
                ok: true,
                msg: 'Producto No encontrado',
                id
            });
        }

        const cambiosProducto = {
            ...req.body,
        }

        const productoActualizado = await Producto.findByIdAndUpdate(id, cambiosProducto, { new: true });

        res.json({
            ok: true,
            msg: 'Producto actualizado',
            Producto: productoActualizado
        });

    } catch (error) {
        res.status(500).json({
            ok: false,
            msg: 'Hubo un error en el servidor'
        })
    }

}

module.exports = {
    getProductos,
    crearProducto,
    eliminarProducto,
    actualizarProducto
}