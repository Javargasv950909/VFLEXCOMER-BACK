const { response } = require('express');
const bcrypt = require('bcryptjs');
const Usuario = require('../Models/usuario');
const { generarJWT } = require('../Helpers/jwt');


const crearUsuario = async(req, res = response) => {

    const {
        id_plan,
        id_tienda,
        fecha_registro,
        nombre_propietario,
        apellido_propietario,
        nombre_establecimiento,
        role,
        usuario,
        contrasena,
        estado_usuario
    } = req.body;

    try {

        // Validacion existe usuario
        const existeUsuario = await Usuario.findOne({ usuario });

        if (existeUsuario) {
            return res.status(400).json({
                ok: false,
                msg: 'Usuario ya registrado'
            });
        }

        const nuevoUsuario = new Usuario(req.body);

        //Encriptar password
        const pass = bcrypt.genSaltSync();
        nuevoUsuario.contrasena = bcrypt.hashSync(contrasena, pass);

        //Guardar ususario
        await nuevoUsuario.save();

        //Generar el JWT
        const token = await generarJWT(nuevoUsuario.id);

        res.json({
            ok: true,
            nuevoUsuario,
            token
        });

    } catch (error) {
        console.log(error);
        res.status(500).json({
            ok: false,
            msg: 'Error inesperado  --> crear Usuario... ver logs'
        })
    }
}

const getUsuarios = async(req, res) => {

    const usuarios = await Usuario.find({}, 'id id_tienda fecha_registro nombre_propietario apellido_propietario nombre_establecimiento role usuario contrasena estado_usuario');
    res.json({
        ok: true,
        usuarios,
        id: req.id
    });
}


const actualizarUsuario = async(req, res = response) => {

    const usuarioid = req.params.id;

    try {

        // Busqueda de usuario por id
        const usuarioDB = await Usuario.findById(usuarioid);

        if (!usuarioDB) {
            return res.status(400).json({
                ok: false,
                msg: 'No existe un usuario con este ID'
            });
        }

        //Campos a actualizar
        const { fecha_registro, contrasena, usuario, ...campos } = req.body;

        if (usuarioDB.usuario != usuario) {
            const existeUsuario = await Usuario.findOne({ usuario });
            if (existeUsuario) {
                return res.status(400).json({
                    ok: false,
                    msg: 'Ya existe un usuario con ese nombre de usuario / correo'
                });
            }

        }

        campos.usuario = usuario;
        // Actualizar usuario 
        const usuarioActualizado = await Usuario.findByIdAndUpdate(usuarioid, campos, { new: true });

        res.json({
            ok: true,
            usuario: usuarioActualizado
        });

    } catch (error) {
        res.status(500).json({
            ok: false,
            msg: 'Error inesperado  --> actualizar Usuario... ver logs'
        });
    }
}

const eliminarUsuario = async(req, res = response) => {

    const usuarioid = req.params.id;

    try {
        //Busqueda de usuario por ID
        const usuarioDB = await Usuario.findById(usuarioid);

        if (!usuarioDB) {
            return res.status(400).json({
                ok: false,
                msg: 'No existe un usuario con este ID'
            });
        }

        //Eliminar usuario de BD
        /*
        await Usuario.findByIdAndDelete(usuarioid);
        res.status(500).json({
            ok: true,
            msg: 'Usuario eliminado correctamente'
        });*/

        // Actualizar estado del usuario
        await Usuario.findOneAndUpdate({ _id: usuarioid }, { estado_usuario: false }, { new: true });
        res.status(500).json({
            ok: true,
            msg: 'Usuario eliminado correctamente'
        });

    } catch (error) {
        res.status(500).json({
            ok: false,
            msg: 'No se pudo eliminar el usuario... ver logs'
        });
    }
}


module.exports = {
    getUsuarios,
    crearUsuario,
    actualizarUsuario,
    eliminarUsuario
}