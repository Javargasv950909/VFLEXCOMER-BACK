const { response } = require('express');
const Venta = require('../Models/venta');
const Tienda = require('../Models/tienda');
const Producto = require('../Models/producto');


const getVentas = (req, res) => {


     try {
          Venta.find({}).populate({path:'id_tienda'}).populate({ path:'productos.productos'}).exec((err, ventas) => {
                if(err) {
                    res.status(500).json({
                        ok: false,
                        msg: "Error"+ err,
                    });
                } else {
                    res.json({
                        ok: true,
                        ventas
                    });
                }
            })
     } catch (err) {
         res.status(500).json({
             ok: false,
             msg: 'Hubo un error en el servidor',
             err
         })
     }

}

const guardarVenta = async(req, res) => {

    try {
        const body = req.body;

        validar_cantidad(body.productos_detalle, (respuesta) => {
            if(respuesta == false) 
               return res.json({
                    ok: false,
                    msg: 'No hay suficientes productos en stock'
                });

            let venta = new Venta({
                valor_total: body.precio_total_venta,
                id_tienda: body.id_tienda,
                productos: respuesta
            });

            venta.save((err, NuevaVenta) => {
                res.json({
                    ok: true,
                    NuevaVenta
                 });
            })
        });

    } catch (error) {
        res.status(500).json({
           ok: false,
           msg: 'Hubo un error en el servidor'
       })
    }

}

const validar_cantidad = async (productos, callback) => {

    let productos_id = [];
    
    productos.forEach(element => {
        productos_id.push(element.producto_id);
    });

    let  respuestaProductos = [];

    Producto.find({})
    .where("_id").in(productos_id)
    .exec(async (err,data) => {
        for(let i = 0; i < data.length; i++){

            let cantidad = productos.find(p => p.producto_id == data[i]._id).cantidad;

            if(cantidad <= data[i].cantidad) {
                cantidadNueva = data[i].cantidad - cantidad;
                let modifico = await Producto.findByIdAndUpdate(data[i]._id, {
                    cantidad: cantidadNueva
                });

                if(modifico != false ){
                    respuestaProductos.push({
                        productos: data[i]._id,
                        cantidad: cantidad
                    });
                }
            } 
        }
        callback(respuestaProductos.length == 0 ? false : respuestaProductos);
    })

}

    

module.exports = {
    getVentas,
    guardarVenta
}