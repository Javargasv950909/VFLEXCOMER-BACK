const { response } = require('express');
const Compra = require('../Models/compra');
const Producto = require('../Models/producto');
const Tienda = require('../Models/tienda');


const getCompras = async(req, res) => {

    const resp = Tienda.aggregate(
        [
            {
                $lookup: 
                {
                   from: "tiendas",
                   localField: "id_tienda",
                   foreignField: "_id",
                   as: "compraTienda"
                }
            }
        ]
    )

    res.json({
        ok: true,
        resp
    });

    console.log("resultado de compra", JSON.stringify(resp));

}

module.exports = {
    getCompras
}