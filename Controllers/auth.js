const { response } = require('express');
const Usuario = require('../Models/usuario');
const bcrypt = require('bcryptjs');
const { generarJWT } = require('../Helpers/jwt');


const login = async(req, res = response) => {

    const { usuario, contrasena } = req.body;

    try {

        // verificar usuario
        const usuarioDB = await Usuario.findOne({ usuario });

        if (!usuarioDB) {
            return res.status(404).json({
                ok: false,
                msg: 'Usuario no valido'
            });
        }

        // verificar contraseña
        const validContrasena = bcrypt.compareSync(contrasena, usuarioDB.contrasena);

        if (!validContrasena) {
            return res.status(400).json({
                ok: false,
                msg: 'Contraseña no valida'
            });
        }

        //Generar el JWT
        const token = await generarJWT(usuarioDB.id);

        res.status(500).json({
            ok: true,
            token,
            msg: 'Hola mundo'
        });

    } catch (error) {
        return res.status(500).json({
            ok: false,
            msg: 'Hable con el administrador'
        });
    }


}

module.exports = {
    login
}