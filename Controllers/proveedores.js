const { response } = require('express');
const Proveedor = require('../Models/proveedor');
const Tienda = require('../Models/tienda');

const crearProveedor = async(req, res) => {
    const {
        id_tienda,
        nombre_proveedor,
        contacto_proveedor,
        dia_promedio
    } = req.body;

    try {

        // Validacion existe proveedor
        const exitoProveedor = await Proveedor.findOne({ nombre_proveedor });

        if (exitoProveedor) {
            return res.status(400).json({
                ok: false,
                msg: 'proveedor ya existente'
            });
        }

        const nuevoProveedor = new Proveedor(req.body);

        //Guardar proveedor
        await nuevoProveedor.save();

        res.json({
            ok: true,
            nuevoProveedor
        });

    } catch (error) {
        console.log(error);
        res.status(500).json({
            ok: false,
            msg: 'Error inesperado  --> crear proveedor... ver logs'
        })
    }
}


const getProveedores = async(req, res) => {

    Proveedor.find({}, function(err, tienda) {
        Tienda.populate(tienda, { path: "tienda" }, function(err, tienda) {
            res.status(200).send(tienda);
        });
    });

}

module.exports = {
    getProveedores,
    crearProveedor
}