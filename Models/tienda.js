const { Schema, model } = require('mongoose');

const TiendaSchema = Schema({
    fecha_registro: {
        type: Date,
        required: true
    },
    nombre_tienda: {
        type: String,
        required: true
    },
    descripcion_tienda: {
        type: String,
        required: true
    },
    direccion_tienda: {
        type: String,
        required: true
    },
    departamento: {
        type: String,
        required: true
    },
    ciudad: {
        type: String,
        required: true
    },
    nit: {
        type: String,
        required: true
    },
    estado_tienda: {
        type: Boolean,
        required: true
    }
});

TiendaSchema.method('toJSON', function() {
    const { __v, _id, ...object } = this.toObject();
    object.id = _id;
    return object;
});

module.exports = model('Tienda', TiendaSchema);