const { Schema, model } = require('mongoose');
const mongoose = require('mongoose');


const VentaSchema = Schema({
    id_tienda: {
        type: Schema.Types.ObjectId,
        ref: 'Tienda',
        required: true
    },
    fecha_venta: {
        type: Date,
        default: Date.now()
    },
    valor_total: {
        type: Number,
        required: true
    },
    productos: [{
        productos: {
            type: Schema.Types.ObjectId,
            ref: 'Producto'
        },
        cantidad: Number
    }]
});

VentaSchema.method('toJSON', function() {
    const { __v, _id, ...object } = this.toObject();
    object.id = _id;
    return object;
});

module.exports = model('Venta', VentaSchema);