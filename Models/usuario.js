const { Schema, model } = require('mongoose');

const UsuarioSchema = Schema({
    id_plan: {
        type: Schema.Types.ObjectId,
        ref: 'Planes',
        required: true
    },
    id_tienda: {
        type: String,
        required: true
    },
    fecha_registro: {
        type: Date,
        required: true
    },
    nombre_propietario: {
        type: String,
        required: true
    },
    apellido_propietario: {
        type: String
    },
    nombre_establecimiento: {
        type: String,
        required: true
    },
    role: {
        type: String,
        required: true,
        default: 'USER_ROLE'
    },
    usuario: {
        type: String,
        required: true
    },
    contrasena: {
        type: String,
        required: true
    },
    estado_usuario: {
        type: Boolean,
        required: true
    }
});

UsuarioSchema.method('toJSON', function() {
    const { __v, _id, ...object } = this.toObject();
    object.id = _id;
    return object;
});

module.exports = model('Usuario', UsuarioSchema);