const { Schema, model } = require('mongoose');

const ProveedoresSchema = Schema({
    id_tienda: {
        type: String,
        // type: Schema.ObjectId,
        // ref: 'tienda',
        required: true
    },
    nombre_proveedor: {
        type: String,
        required: true
    },
    contacto_proveedor: {
        type: String,
        required: true
    },
    dia_promedio: {
        type: String
    }
});

ProveedoresSchema.method('toJSON', function() {
    const { __v, _id, ...object } = this.toObject();
    object.id = _id;
    return object;
});

module.exports = model('Proveedore', ProveedoresSchema);