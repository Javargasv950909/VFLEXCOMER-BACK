const { Schema, model } = require('mongoose');
const mongoose = require('mongoose');

const CompraSchema = Schema({
    id_tienda: {
        type: mongoose.Schema.ObjectId,
        ref: 'tienda',
        required: true
    },
    fecha_compra: {
        type: Date,
        required: true
    },
    proveedor: {
        type: String,
    },
    productos: [{
        type: mongoose.Schema.ObjectId,
        ref: 'producto',
        required: true
    }],
    cantidad: {
        type: Number,
        required: true
    },
    precio_compra: {
        type: Number,
        required: true
    }
});

CompraSchema.method('toJSON', function() {
    const { __v, _id, ...object } = this.toObject();
    object.id = _id;
    return object;
});

module.exports = model('Compra', CompraSchema);