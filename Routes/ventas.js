/*
Ruta: /api/compras
*/
const { Router } = require('express');

const { getVentas, guardarVenta } = require('../Controllers/ventas');


const router = Router();

router.get('/', getVentas);
router.post('/', guardarVenta);


module.exports = router;