/*
Ruta: /api/login
*/

const { Router } = require('express');
const { login } = require('../Controllers/auth');
const { validarCampos } = require('../Middlewares/validar-campos');
const { check } = require('express-validator');


const router = Router();

router.post('/', [
        check('usuario', 'El usuario es obligatorio').not().isEmpty(),
        check('contrasena', 'La contraseña es obligatoria').not().isEmpty(),
        validarCampos
    ],
    login
);



module.exports = router;