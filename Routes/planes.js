/*
Ruta: /api/planes
*/
const { Router } = require('express');

const { getPlanes, crearPlanes, eliminarPlanes } = require('../Controllers/planes');


const router = Router();

router.get('/', getPlanes);
router.post('/', crearPlanes);
router.delete('/:id', eliminarPlanes);


module.exports = router;