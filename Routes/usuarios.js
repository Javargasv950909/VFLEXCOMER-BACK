/*
Ruta: /api/usuarios
*/
const { Router } = require('express');

const { getUsuarios, crearUsuario, actualizarUsuario, eliminarUsuario } = require('../Controllers/usuarios');
const { validarJWT } = require('../Middlewares/validar-jwt');


const router = Router();

router.get('/', validarJWT, getUsuarios);
router.post('/', crearUsuario);
router.put('/:id', validarJWT, actualizarUsuario);
router.delete('/:id', validarJWT, eliminarUsuario);


module.exports = router;