/*
Ruta: /api/compras
*/
const { Router } = require('express');

const { getCompras } = require('../Controllers/compras');


const router = Router();

router.get('/', getCompras);


module.exports = router;