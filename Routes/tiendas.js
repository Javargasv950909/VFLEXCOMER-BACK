/*
Ruta: /api/tiendas
*/
const { Router } = require('express');

const { getTiendas, crearTienda, eliminarTienda, actualizarTienda } = require('../Controllers/tiendas');
const { validarJWT } = require('../Middlewares/validar-jwt');


const router = Router();

router.get('/', validarJWT, getTiendas);
router.post('/', validarJWT, crearTienda);
router.delete('/:id', validarJWT, eliminarTienda);
router.put('/:id', validarJWT, actualizarTienda);


module.exports = router;