/*
Ruta: /api/productos
*/
const { Router } = require('express');

const { getProductos, crearProducto, eliminarProducto, actualizarProducto } = require('../Controllers/productos');


const router = Router();

router.get('/', getProductos);
router.post('/', crearProducto);
router.delete('/:id', eliminarProducto);
router.put('/:id', actualizarProducto);


module.exports = router;