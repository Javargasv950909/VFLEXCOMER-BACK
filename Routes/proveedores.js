/*
Ruta: /api/proveedores
*/

const { Router } = require('express');

const { getProveedores, crearProveedor } = require('../Controllers/proveedores');

const router = Router();

router.get('/', getProveedores);
router.post('/', crearProveedor);


module.exports = router;